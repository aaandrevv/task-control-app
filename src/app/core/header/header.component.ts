import {Component, OnDestroy, OnInit} from '@angular/core';
import {map, Subscription} from "rxjs";
import {Store} from "@ngrx/store";
import * as fromApp from "../../store/app.reducer";
import * as UserActions from "../auth/store/user.actions";
import { User } from '../auth/user.model';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, OnDestroy {
  public isAuthenticated: boolean = false;
  private userSubscription: Subscription;
  public user: User | null;
  public isDropdownOpen: boolean;
  public isSettingsOpen: boolean;
  private file: File;

  constructor(private store: Store<fromApp.AppState>) {}

  ngOnInit(): void {
    this.userSubscription = this.store.select('user').pipe(
      map(authState => authState.user)
    ).subscribe(user => {
      this.isAuthenticated = !!user;
      this.user = user;
    });
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  onLogout() {
    this.store.dispatch(UserActions.logout());
  }

  onSubmit(form: NgForm) {
    if (form.valid && this.user) {
      const fd = new FormData();
      if (this.file) {
        fd.append('image', this.file, this.file.name);
      }
      fd.append('username', form.value["username"]);
      fd.append('email', form.value["email"]);
      fd.append('password', form.value["password"]);
      this.store.dispatch(UserActions.editUser({fd}));
    }
  }

  setFile(event: any) {
    this.file = event.target.files[0];
  }

  handleErrorImage(event: Event) {
    (event.target as HTMLImageElement).src = "../../../assets/unauth.webp";
  }
}
