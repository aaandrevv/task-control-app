import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";

export interface IResponseData {
  id: string,
  username: string,
  email: string,
  image?: string
}

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private _apiBase = 'https://task-controll-server.onrender.com/api/auth';

  constructor(private http: HttpClient) {}

  login(email: string, password: string) {
    return this.http.post<IResponseData>(`${this._apiBase}/login`, {
      email,
      password
    }, {
      withCredentials: true
    });
  }

  signUp(username: string, email: string, password: string) {
    return this.http.post<IResponseData>(`${this._apiBase}/sign-up`, {
      username,
      email,
      password
    }, {
      withCredentials: true
    })
  }

  isAuthenticated() {
    return this.http.get<{
      status: string,
      user: IResponseData
    }>(`${this._apiBase}/authenticated`, {
      withCredentials: true
    })
  }

  logout() {
    return this.http.get<{ message: string }>(`${this._apiBase}/logout`, {
      withCredentials: true
    })
  }
}
