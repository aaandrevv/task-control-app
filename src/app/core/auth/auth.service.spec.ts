import {inject, TestBed} from '@angular/core/testing';

import {AuthService, IResponseData} from './auth.service';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AuthService]
    });
  });

  it('should be created', inject([AuthService], (service: AuthService) => {
    expect(service).toBeTruthy();
  }));

  it('should login', inject([AuthService, HttpTestingController], (service: AuthService, server: HttpTestingController) => {
    const mockUser: IResponseData = {
      id: "1",
      email: "user@gmail.com",
      username: "user",
      image: "image"
    }
    service.login(mockUser.email, "12345").subscribe(user => {
      expect(user).toEqual(mockUser);
    });

    server.expectOne({
      method: 'POST',
      url: 'http://localhost:8080/api/auth/login',
    }).flush(mockUser);
  }));

  it('should sign up', inject([AuthService, HttpTestingController], (service: AuthService, server: HttpTestingController) => {
    const mockUser: IResponseData = {
      id: "1",
      email: "user@gmail.com",
      username: "user",
      image: "image"
    }
    service.signUp(mockUser.username, mockUser.email, "12345").subscribe(user => {
      expect(user).toEqual(mockUser);
    });

    server.expectOne({
      method: 'POST',
      url: 'http://localhost:8080/api/auth/sign-up',
    }).flush(mockUser)
  }));

  it('should logout', inject([AuthService, HttpTestingController], (service: AuthService, server: HttpTestingController) => {
    const mockMessage = {
      message: "Success"
    };
    service.logout().subscribe(message => {
      expect(message).toEqual(mockMessage);
    });

    server.expectOne({
      method: 'GET',
      url: 'http://localhost:8080/api/auth/logout',
    }).flush(mockMessage)
  }));
});
