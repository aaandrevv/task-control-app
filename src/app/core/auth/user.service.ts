import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {User} from "./user.model";

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private _apiBase = 'https://task-controll-server.onrender.com/api/users';

  constructor(private http: HttpClient) {}

  edit(fd: FormData) {
    return this.http.put<{ user: User}>(this._apiBase, fd, {
      withCredentials: true
    })
  }
}
