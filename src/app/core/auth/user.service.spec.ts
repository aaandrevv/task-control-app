import {inject, TestBed} from '@angular/core/testing';

import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {UserService} from "./user.service";
import {User} from "./user.model";

describe('UserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [UserService]
    });
  });

  it('should be created', inject([UserService], (service: UserService) => {
    expect(service).toBeTruthy();
  }));

  it('should edit', inject([UserService, HttpTestingController], (service: UserService, server: HttpTestingController) => {
    const mockUser = {
      id: "1",
      email: "newseremail@gmail.com",
      username: "newUsername",
      image: "newImg"
    }
    const formData: FormData = new FormData();
    formData.append('username', mockUser.username);
    formData.append('email', mockUser.email);
    formData.append('image', mockUser.image);

    service.edit(formData).subscribe(response => {
      expect(response.user).toEqual(mockUser);
    });

    server.expectOne({
      method: 'PUT',
      url: 'http://localhost:8080/api/users',
    }).flush({user: mockUser});
  }));
});
