import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Subscription} from "rxjs";
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import * as App from "../../store/app.reducer";
import * as AuthActions from "./store/user.actions";

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AuthComponent implements OnInit, OnDestroy {

  isLogin: boolean = true;
  error: string | null = null;

  storeSubscription: Subscription;

  constructor(private router: Router,
              private store: Store<App.AppState>) {}

  ngOnInit(): void {
    this.storeSubscription = this.store.select('user').subscribe(authState => {
      this.error = authState.authenticationError
    })
  }

  ngOnDestroy(): void {
    this.storeSubscription.unsubscribe();
  }

  onAuthSwitch() {
    this.isLogin = !this.isLogin;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const email = form.value["email"];
    const password = form.value["password"];
    const username = form.value["username"];
    const confirm = form.value["password-confirm"];


    if(this.isLogin) {
      this.store.dispatch(AuthActions.startLogin({email, password}));
    } else {
      if (password !== confirm) {
        return this.store.dispatch(AuthActions.authFail({errorMessage: "Passwords do not match!", redirect: false}));
      }
      this.store.dispatch(AuthActions.startSignUp({username, email, password}));
    }
  }


}
