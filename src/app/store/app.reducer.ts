import * as user from "../core/auth/store/user.reducer";
import * as boards from "../boards/store/boards.reducer";
import {ActionReducerMap} from "@ngrx/store";

export interface AppState {
  user: user.State,
  boards: boards.State,
}

export const appReducer: ActionReducerMap<AppState> = {
  user: user.userReducer,
  boards: boards.boardsReducer,
}
