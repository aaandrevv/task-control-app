import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {Store} from "@ngrx/store";
import * as App from "./store/app.reducer"
import * as AuthActions from "./core/auth/store/user.actions";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {


  title = 'task-control';
  constructor(private router: Router,
              private store: Store<App.AppState>) {}

  ngOnInit(): void {
    this.store.dispatch(AuthActions.initAuth());
  }

}
