import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import * as App from "../store/app.reducer"
import * as BoardsActions from "./store/boards.actions"
import {Board} from "./board.model";
import {map, Subscription} from "rxjs";
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-boards',
  templateUrl: './boards.component.html',
  styleUrls: ['./boards.component.scss'],
})
export class BoardsComponent implements OnInit, OnDestroy {
  isModalOpen: boolean;
  boards: Board[];
  boardsSubscription: Subscription;
  keywords: string;
  sorting: string = "created date";
  order: 'ASC' | 'DESC' = 'ASC';

  constructor(private store: Store<App.AppState>) { }

  ngOnInit(): void {
    this.boardsSubscription = this.store.select('boards').pipe(
      map(boardsState => boardsState.boards)
    ).subscribe((boards: Board[]) => {
      this.boards = boards;
    })
  }


  ngOnDestroy(): void {
    this.boardsSubscription.unsubscribe();
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.store.dispatch(BoardsActions.addBoard({board: form.value}));
  }
}
