import { Injectable } from '@angular/core';
import {
  Router, Resolve,
  RouterStateSnapshot,
  ActivatedRouteSnapshot
} from '@angular/router';
import {map, Observable, of, switchMap, take} from 'rxjs';
import {Board} from "./board.model";
import {Actions, ofType} from "@ngrx/effects";
import {Store} from "@ngrx/store";
import {AppState} from "../store/app.reducer";
import * as BoardsActions from "./store/boards.actions";

@Injectable({
  providedIn: 'root'
})
export class BoardsResolver implements Resolve<{boards: Board[]}> {

  constructor(private actions$: Actions,
              private store: Store<AppState>) {}

  resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<{ boards: Board[] }> | Promise<{ boards: Board[] }> | { boards: Board[] } {
    return this.store.select('boards').pipe(
      take(1),
      map(boardsState => {
        return boardsState.boards;
      }),
      switchMap(boards => {
        if (boards.length === 0) {
          this.store.dispatch(BoardsActions.fetchBoards());
          return this.actions$.pipe(
            ofType(BoardsActions.setBoards),
            take(1)
          );
        } else {
          return of({boards});
        }
      })
    )
  }

}
