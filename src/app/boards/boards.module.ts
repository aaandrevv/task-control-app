import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {BoardItemComponent} from "./board-item/board-item.component";
import {BoardsComponent} from "./boards.component";
import {TasksComponent} from "./tasks/tasks.component";
import {FormsModule} from "@angular/forms";
import {BoardListItemComponent} from "./board-list-item/board-list-item.component";
import { TaskComponent } from './tasks/task/task.component';
import { ArchivedPipe } from './tasks/archived.pipe';
import {SharedModule} from "../shared/shared.module";
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";
import { CommentComponent } from './tasks/comment/comment.component';
import { CommentsComponent } from './tasks/comments/comments.component';
import {BoardsRoutingModule} from "./boards-routing.module";


@NgModule({
  declarations: [
    BoardItemComponent,
    BoardsComponent,
    BoardListItemComponent,
    TasksComponent,
    TaskComponent,
    ArchivedPipe,
    CommentComponent,
    CommentsComponent
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    SharedModule,
    BoardsRoutingModule
  ]
})
export class BoardsModule {
}
