import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {BoardsComponent} from "./boards.component";
import {AuthGuard} from "../core/auth/auth.guard";
import {BoardsResolver} from "./boards.resolver";
import {BoardItemComponent} from "./board-item/board-item.component";

const routes: Routes = [
  {path: '', component: BoardsComponent, canActivate: [AuthGuard], resolve: [BoardsResolver]},
  {path: ':id', component: BoardItemComponent, canActivate: [AuthGuard], resolve: [BoardsResolver]}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class BoardsRoutingModule { }
