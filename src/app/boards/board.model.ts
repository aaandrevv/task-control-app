import {Task} from "./tasks/task.model";

export interface Board {
  _id: string,
  name: string,
  description: string,
  createdBy: string,
  tasks: Task[],
  createdAt: Date,
  updatedAt: Date,
  todoColor: string,
  inProgressColor: string,
  doneColor: string
}
