import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Task} from "../task.model";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.reducer";
import * as BoardsActions from "../../store/boards.actions";
import {faBoxArchive, faPen} from '@fortawesome/free-solid-svg-icons';
import {NgForm} from "@angular/forms";

@Component({
  selector: 'app-task',
  templateUrl: './task.component.html',
  styleUrls: ['./task.component.scss'],
})
export class TaskComponent implements OnInit {
  private file: File;
  faPen = faPen;
  faBoxArchive = faBoxArchive;
  onHover: boolean;

  @Input() task: Task;
  @Input() boardId: string;
  @Output() taskDragged = new EventEmitter<Task>();
  isTaskModalOpen: boolean;
  isEditModalOpen: boolean;

  constructor(private store: Store<AppState>) {}

  ngOnInit(): void {}

  deleteTask() {
    this.store.dispatch(BoardsActions.deleteTaskStart({boardId: this.boardId, taskId: this.task._id}));
  }

  setFile(event: any) {
    this.file = event.target.files[0];
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    const fd = new FormData();
    if (this.file) {
      fd.append('image', this.file, this.file.name);
    }
    fd.append('name', form.value["name"]);
    fd.append('description', form.value["description"]);
    this.store.dispatch(BoardsActions.editTask({boardId: this.boardId, taskId: this.task._id, formData: fd}));
  }

  archiveTask() {
    this.store.dispatch(BoardsActions.archiveTask({boardId: this.boardId, taskId: this.task._id, archive: !this.task.isArchived}));
  }
}
