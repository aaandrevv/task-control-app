import { Pipe, PipeTransform } from '@angular/core';
import {Task} from "./task.model";

@Pipe({
  name: 'archived'
})
export class ArchivedPipe implements PipeTransform {

  transform(items: Task[], isArchived: boolean): Task[] {
    if (!items) {
      return items;
    }
    return items.filter(task => task.isArchived === isArchived);
  }
}
