import {
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import {NgForm} from "@angular/forms";
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.reducer";

import * as BoardsActions from "../store/boards.actions";
import {Task} from "./task.model";
import {faPalette} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-tasks',
  templateUrl: './tasks.component.html',
  styleUrls: ['./tasks.component.scss'],
})
export class TasksComponent implements OnInit {
  faPalette = faPalette;

  @Input() tasks: Task[];
  @Input() statusName: string;
  @Input() boardId: string;
  @Input() defaultColor: string;
  @Input() isArchived: boolean;
  @Output() transferDraggedTask = new EventEmitter<Task>();
  @Output() dropTask = new EventEmitter<string>();

  color: string;
  isModalOpen: boolean;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.color = this.defaultColor;
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.store.dispatch(BoardsActions.addTaskStart({
      name: form.value['name'],
      description: form.value['description'],
      status: this.statusName,
      boardId: this.boardId
    }))
  }

  changeColor() {
    this.store.dispatch(BoardsActions.changeContainerColor({
      boardId: this.boardId,
      container: this.statusName,
      color: this.color
    }));
  }

}
