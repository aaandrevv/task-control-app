import {ChangeDetectionStrategy, Component, Input, OnDestroy, OnInit} from '@angular/core';
import {Task} from "../task.model";
import {NgForm} from "@angular/forms";
import * as BoardsActions from "../../store/boards.actions";
import {Store} from "@ngrx/store";
import {AppState} from "../../../store/app.reducer";
import {Subscription} from "rxjs";
import {User} from 'src/app/core/auth/user.model';
import {Comment} from "../comment.model";

@Component({
  selector: 'app-comments',
  templateUrl: './comments.component.html',
  styleUrls: ['./comments.component.scss'],
})
export class CommentsComponent implements OnInit, OnDestroy {

  @Input() taskId: string;
  @Input() boardId: string;

  comments: Comment[];
  taskSub: Subscription;
  userSub: Subscription;

  user: User;
  task: Task;

  constructor(private store: Store<AppState>) {
  }

  ngOnInit(): void {
    this.taskSub = this.store.select('boards').subscribe(
      boardsState => boardsState.boards.forEach(board => {
        if (board._id === this.boardId) {
          board.tasks.forEach(task => {
            if (task._id === this.taskId) {
              this.task = task;
              this.comments = [...task.comments];
              return;
            }
          })
        }
      })
    );
    this.userSub = this.store.select('user').subscribe(
      userState => {
        if (userState.user) {
          this.user = userState.user
        }
      }
    )
  }

  ngOnDestroy(): void {
    this.taskSub.unsubscribe();
    this.userSub.unsubscribe();
  }

  onPost(form: NgForm) {
    this.store.dispatch(BoardsActions.postComment({
      boardId: this.boardId,
      taskId: this.task._id,
      comment: form.value["comment"]
    }));
    this.comments.push({comment: form.value["comment"], createdAt: new Date(), createdBy: this.user});
  }
}
