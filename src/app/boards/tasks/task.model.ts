import {Comment} from "./comment.model";

export interface Task {
  _id: string,
  name: string,
  description: string,
  status: string,
  isArchived: boolean,
  createdAt: Date,
  comments: Comment[]
  image?: string,
}
