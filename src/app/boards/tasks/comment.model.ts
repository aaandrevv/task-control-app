import {User} from "../../core/auth/user.model";

export interface Comment {
  createdBy: User,
  comment: string,
  createdAt: Date
}
