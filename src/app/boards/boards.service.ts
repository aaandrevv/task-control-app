import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Board} from "./board.model";
import {Task} from "./tasks/task.model";
import {Comment} from "./tasks/comment.model";

@Injectable({
  providedIn: 'root'
})
export class BoardsService {
  private _apiBase = "https://task-controll-server.onrender.com/api/boards"

  constructor(private http: HttpClient) {}

  fetchBoards() {
    return this.http.get<{ boards: Board[] }>(`${this._apiBase}`, {
      withCredentials: true
    })
  }

  addBoard(board: Board) {
    return this.http.post<{ boards: Board[] }>(`${this._apiBase}`, {
      name: board.name,
      description: board.description
    }, {
      withCredentials: true
    });
  }

  editBoard(board: Board) {
    return this.http.patch<{ boards: Board[] }>(`${this._apiBase}/${board._id}`, {
      name: board.name,
      description: board.description
    }, {
      withCredentials: true
    });
  }

  deleteBoard(boardId: string) {
    return this.http.delete<{ boards: Board[] }>(`${this._apiBase}/${boardId}`, {
      withCredentials: true
    });
  }

  addTask(name: string, description: string, status: string, boardId: string) {
    return this.http.post<{ board: Board }>(`${this._apiBase}/${boardId}/tasks`, {
      name,
      description,
      status
    }, {
      withCredentials: true
    });
  }

  deleteTask(boardId: string, taskId: string) {
    return this.http.delete<{ board: Board }>(`${this._apiBase}/${boardId}/tasks/${taskId}`, {
      withCredentials: true
    });
  }

  changeTaskStatus(boardId: string, taskId: string, status: string) {
    return this.http.patch<{ task: Task, boardId: string }>(`${this._apiBase}/${boardId}/tasks/${taskId}`, {
      status: status.toUpperCase()
    }, {
      withCredentials: true
    });
  }

  editTask(boardId: string, taskId: string, formData: FormData) {
    return this.http.patch<{ task: Task, boardId: string }>(`${this._apiBase}/${boardId}/tasks/${taskId}`, formData, {
      withCredentials: true
    });
  }

  changeColor(boardId: string, containerName: string, color: string) {
    let colorProp: string;
    switch (containerName) {
      case 'TODO': {
        colorProp = 'todoColor';
        break;
      }
      case 'IN PROGRESS': {
        colorProp = 'inProgressColor';
        break;
      }
      case 'DONE': {
        colorProp = 'doneColor';
        break;
      }
      default: {
        colorProp = 'todoColor';
        break;
      }
    }

    return this.http.patch<{ boards: Board[] }>(`${this._apiBase}/${boardId}`, {
      [colorProp]: color
    }, {
      withCredentials: true
    });
  }

  postComment(boardId: string, taskId: string, comment: string) {
    return this.http.patch<{ comment: Comment, boardId: string, taskId: string }>
    (`${this._apiBase}/${boardId}/tasks/${taskId}/comments`, {
      comment
    }, {
      withCredentials: true
    });
  }

  archiveTask(boardId: string, taskId: string, isArchived: boolean) {
    return this.http.patch<{task: Task, boardId: string}>(`${this._apiBase}/${boardId}/tasks/${taskId}`, {
      isArchived
    }, {
      withCredentials: true
    });
  }
}
