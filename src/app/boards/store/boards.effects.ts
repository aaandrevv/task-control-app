import {act, Actions, createEffect, ofType} from "@ngrx/effects";
import {HttpClient} from "@angular/common/http";
import * as BoardsActions from "./boards.actions";
import {debounceTime, distinctUntilChanged, map, switchMap} from "rxjs";
import {Injectable} from "@angular/core";
import {BoardsService} from "../boards.service";

@Injectable()
export class BoardsEffects {

  constructor(private actions$: Actions,
              private http: HttpClient,
              private service: BoardsService) {
  }

  fetchBoards$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.fetchBoards),
      switchMap(() => {
        return this.service.fetchBoards().pipe(
          map(boards => {
            return BoardsActions.setBoards(boards);
          })
        );
      })
    )
  );

  addBoard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.addBoard),
      switchMap(action => {
        return this.service.addBoard(action.board).pipe(
          map(boards => {
            return BoardsActions.setBoards(boards);
          })
        )
      })
    )
  );

  editBoard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.editBoard),
      switchMap(action => {
        return this.service.editBoard(action.board).pipe(
          map(boards => {
            return BoardsActions.setBoards(boards);
          })
        )
      })
    )
  )

  deleteBoard$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.deleteBoard),
      switchMap(action => {
        return this.service.deleteBoard(action.boardId).pipe(
          map(boards => {
            return BoardsActions.setBoards(boards);
          })
        )
      })
    )
  );

  addTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.addTaskStart),
      switchMap(action => {
        return this.service.addTask(action.name, action.description, action.status, action.boardId).pipe(
          map(board => {
            return BoardsActions.taskOperationSuccess(board);
          })
        )
      })
    )
  );

  deleteTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.deleteTaskStart),
      switchMap(action => {
        return this.service.deleteTask(action.boardId, action.taskId).pipe(
          map(board => {
            return BoardsActions.taskOperationSuccess(board);
          })
        )
      })
    )
  )

  editTaskStatus$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.changeTaskStatusStart),
      switchMap(action => {
        return this.service.changeTaskStatus(action.boardId, action.taskId, action.status).pipe(
          map(() => {
            return BoardsActions.changeTaskStatusSuccess();
          })
        )
      })
    )
  )

  editTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.editTask),
      switchMap(action => {
        return this.service.editTask(action.boardId, action.taskId, action.formData).pipe(
          map(response => {
            return BoardsActions.editTaskSuccess({task: response.task, boardId: response.boardId});
          })
        )
      })
    )
  )

  changeColor$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.changeContainerColor),
      distinctUntilChanged(),
      debounceTime(1000),
      switchMap(action => {
        return this.service.changeColor(action.boardId, action.container, action.color).pipe(
          map(() => {
            return BoardsActions.changeContainerColorSuccess();
          })
        )
      })
    )
  )

  postComment$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.postComment),
      switchMap(action => {
        return this.service.postComment(action.boardId, action.taskId, action.comment).pipe(
          map(action => {
            return BoardsActions.postCommentSuccess({
              comment: action.comment,
              boardId: action.boardId,
              taskId: action.taskId
            });
          })
        )
      })
    )
  )

  archiveTask$ = createEffect(() =>
    this.actions$.pipe(
      ofType(BoardsActions.archiveTask),
      switchMap(action => {
        return this.service.archiveTask(action.boardId, action.taskId, action.archive).pipe(
          map(action => {
            return BoardsActions.archiveTaskSuccess({boardId: action.boardId, task: action.task});
          })
        )
      })
    )
  )
}
