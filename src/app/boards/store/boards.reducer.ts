import {Action, createReducer, on} from "@ngrx/store";
import {Board} from "../board.model";
import * as AuthActions from "../../core/auth/store/user.actions";
import * as BoardsActions from "./boards.actions";

export interface State {
  boards: Board[]
}

const initialState: State = {
  boards: []
}

const _boardsReducer = createReducer(
  initialState,
  on(BoardsActions.setBoards, (state, action) => {
    return {
      ...state,
      boards: [...action.boards]
    }
  }),
  on(AuthActions.logout, (state) => {
    return {
      ...state,
      boards: []
    }
  }),
  on(BoardsActions.taskOperationSuccess, (state, action) => {
    return {
      ...state,
      boards: state.boards.map(board => board._id === action.board._id ? {...action.board} : board)
    }
  }),

  on(BoardsActions.changeTaskStatusStart, (state, action) => {
    return {
      ...state,
      boards: state.boards.map(board => {
        if (board._id === action.boardId) {
          return {
            ...board, tasks: board.tasks.map(task => {
              if (task._id === action.taskId) {
                return {...task, status: action.status}
              }
              return task;
            })
          }
        }
        return board;
      })
    }
  }),

  on(BoardsActions.editTaskSuccess, (state, action) => {
    return {
      ...state,
      boards: state.boards.map(board => {
        if (board._id === action.boardId) {
          return {
            ...board, tasks: board.tasks.map(task => {
              if (task._id === action.task._id) {
                return {...task, ...action.task};
              }
              return task;
            })
          }
        }
        return board;
      })
    }
  }),

  on(BoardsActions.archiveTaskSuccess, (state, action) => {
    return {
      ...state,
      boards: state.boards.map(board => {
        if (board._id === action.boardId) {
          return {
            ...board,
            tasks: board.tasks.map(task => {
              if (task._id === action.task._id) {
                return {...task, isArchived: action.task.isArchived}
              }
              return task;
            })
          }
        }
        return board;
      })
    }
  })
);

export function boardsReducer(state: State | undefined, action: Action) {
  return _boardsReducer(state, action);
}
