import {Board} from "../board.model";
import {createAction, props} from "@ngrx/store";
import {Task} from "../tasks/task.model";
import {Comment} from "../tasks/comment.model";

export const fetchBoards = createAction(
  "[Boards] Fetch Boards"
);

export const setBoards = createAction(
  "[Boards] Set Boards",
  props<{boards: Board[]}>()
)

export const addBoard = createAction(
  "[Boards] Add Board",
  props<{board: Board}>()
)

export const deleteBoard = createAction(
  "[Boards] Delete Board",
  props<{boardId: string}>()
)

export const editBoard = createAction(
  "[Boards] Edit Board",
  props<{board: Board}>()
)

export const addTaskStart = createAction(
  "[Boards] Start Adding Task",
  props<{name: string, description: string, status: string, boardId: string}>()
)

export const taskOperationSuccess = createAction(
  "[Boards] Task Operation Success",
  props<{board: Board}>()
)

export const deleteTaskStart = createAction(
  "[Boards] Start Deleting Task",
  props<{boardId: string, taskId: string}>()
)

export const changeTaskStatusStart = createAction(
  "[Boards] Start Change Task Status",
  props<{boardId: string, taskId: string, status: string}>()
)

export const changeTaskStatusSuccess = createAction(
  "[Boards] Success Change Task Status"
)

export const editTask = createAction(
  "[Boards] Edit Task",
  props<{boardId: string, taskId: string, formData: FormData}>()
)

export const editTaskSuccess = createAction(
  "[Boards] Edit Task Success",
  props<{task: Task, boardId: string}>()
)

export const changeContainerColor = createAction(
  "[Boards] Change Container Color",
  props<{boardId: string, container: string, color: string}>()
)

export const changeContainerColorSuccess = createAction(
  "[Boards] Change Container Color Success"
)

export const postComment = createAction(
  "[Comments] Post Comment",
  props<{boardId: string, taskId: string, comment: string}>()
)

export const postCommentSuccess = createAction(
  "[Comments] Post Comment Success",
  props<{boardId: string, taskId: string, comment: Comment}>()
)

export const archiveTask = createAction(
  "[Boards] Archive Task",
  props<{boardId: string, taskId: string, archive: boolean}>()
)

export const archiveTaskSuccess = createAction(
  "[Boards] Archive Task Success",
  props<{task: Task, boardId: string}>()
)
