import {inject, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from "@angular/common/http/testing";
import {BoardsService} from "./boards.service";
import {Board} from "./board.model";
import {Task} from "./tasks/task.model";
import {Comment} from "./tasks/comment.model";

describe('AuthService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [BoardsService]
    });
  });

  it('should be created', inject([BoardsService], (service: BoardsService) => {
    expect(service).toBeTruthy();
  }));

  it('should fetch boards', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const mockBoards: Board[] = [
      {
        _id: "1",
        name: "board1",
        description: "board1 description",
        createdBy: "1",
        tasks: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        todoColor: "#000000",
        inProgressColor: "#000000",
        doneColor: "#000000"
      },
      {
        _id: "2",
        name: "board2",
        description: "board2 description",
        createdBy: "2",
        tasks: [],
        createdAt: new Date(),
        updatedAt: new Date(),
        todoColor: "#ffffff",
        inProgressColor: "#ffffff",
        doneColor: "#ffffff"
      }
    ];

    service.fetchBoards().subscribe(response => {
      expect(response.boards).toEqual(mockBoards);
    });

    server.expectOne({
      method: 'GET',
      url: 'http://localhost:8080/api/boards',
    }).flush({boards: mockBoards});
  }));

  it('should add board', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const mockBoard: Board = {
      _id: "1",
      name: "board1",
      description: "board1 description",
      createdBy: "1",
      tasks: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      todoColor: "#000000",
      inProgressColor: "#000000",
      doneColor: "#000000"
    };
    const mockBoards: Board[] = [mockBoard];

    service.addBoard(mockBoard).subscribe(response => {
      expect(response.boards).toEqual(mockBoards);
    });

    server.expectOne({
      method: 'POST',
      url: 'http://localhost:8080/api/boards',
    }).flush({boards: mockBoards});
  }));

  it('should edit board', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const mockBoard: Board = {
      _id: "1",
      name: "board1",
      description: "board1 description",
      createdBy: "1",
      tasks: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      todoColor: "#000000",
      inProgressColor: "#000000",
      doneColor: "#000000"
    };
    const mockBoards: Board[] = [mockBoard];

    service.editBoard(mockBoard).subscribe(response => {
      expect(response.boards).toEqual(mockBoards);
    });

    server.expectOne({
      method: 'PATCH',
      url: 'http://localhost:8080/api/boards/1',
    }).flush({boards: mockBoards});
  }));

  it('should delete board', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const mockBoards: Board[] = [];
    const mockBoardId = '1';
    service.deleteBoard(mockBoardId).subscribe(response => {
      expect(response.boards).toEqual(mockBoards);
    });

    server.expectOne({
      method: 'DELETE',
      url: `http://localhost:8080/api/boards/${mockBoardId}`,
    }).flush({boards: mockBoards});
  }));

  it('should add task', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const mockTask: Task = {
      _id: '1',
      name: 'task1',
      description: 'task1 description',
      status: 'TODO',
      isArchived: false,
      createdAt: new Date(),
      comments: [],
      image: 'new image',
    }
    const mockBoard: Board = {
      _id: "1",
      name: "board1",
      description: "board1 description",
      createdBy: "1",
      tasks: [mockTask],
      createdAt: new Date(),
      updatedAt: new Date(),
      todoColor: "#000000",
      inProgressColor: "#000000",
      doneColor: "#000000"
    };
    service.addTask(mockTask.name, mockTask.description, mockTask.status, mockBoard._id).subscribe(response => {
      expect(response.board).toEqual(mockBoard);
    });
    server.expectOne({
      method: 'POST',
      url: `http://localhost:8080/api/boards/${mockBoard._id}/tasks`,
    }).flush({board: mockBoard});
  }));

  it('should delete task', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const mockBoard: Board = {
      _id: "1",
      name: "board1",
      description: "board1 description",
      createdBy: "1",
      tasks: [],
      createdAt: new Date(),
      updatedAt: new Date(),
      todoColor: "#000000",
      inProgressColor: "#000000",
      doneColor: "#000000"
    };
    const taskId = "1";
    service.deleteTask(mockBoard._id, taskId).subscribe(response => {
      expect(response.board).toEqual(mockBoard);
    });
    server.expectOne({
      method: 'DELETE',
      url: `http://localhost:8080/api/boards/${mockBoard._id}/tasks/${taskId}`,
    }).flush({board: mockBoard});
  }));

  it('should change task status', inject([BoardsService, HttpTestingController], (service: BoardsService, server: HttpTestingController) => {
    const newStatus = "DONE"
    const mockTask: Task = {
      _id: '1',
      name: 'task1',
      description: 'task1 description',
      status: newStatus,
      isArchived: false,
      createdAt: new Date(),
      comments: [],
      image: 'new image',
    }
    const mockBoard: Board = {
      _id: "1",
      name: "board1",
      description: "board1 description",
      createdBy: "1",
      tasks: [mockTask],
      createdAt: new Date(),
      updatedAt: new Date(),
      todoColor: "#000000",
      inProgressColor: "#000000",
      doneColor: "#000000"
    };
    service.changeTaskStatus(mockBoard._id, mockTask._id, newStatus).subscribe(response => {
      expect(response.task).toEqual(mockTask);
    });
    server.expectOne({
      method: 'PATCH',
      url: `http://localhost:8080/api/boards/${mockBoard._id}/tasks/${mockTask._id}`,
    }).flush({task: mockTask, boardId: mockBoard._id});
  }));
});
