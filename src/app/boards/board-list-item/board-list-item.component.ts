import {ChangeDetectionStrategy, Component, Input, OnInit} from '@angular/core';
import {Board} from "../board.model";
import {AppState} from "../../store/app.reducer";
import * as BoardActions from "../store/boards.actions";
import {Store} from "@ngrx/store";
import {ActivatedRoute, Router} from "@angular/router";
import {NgForm} from "@angular/forms";
import * as BoardsActions from "../store/boards.actions";
import { faGear,  } from '@fortawesome/free-solid-svg-icons';
import { faTrashAlt, faPenToSquare } from '@fortawesome/free-regular-svg-icons';

@Component({
  selector: 'app-board-list-item',
  templateUrl: './board-list-item.component.html',
  styleUrls: ['./board-list-item.component.scss'],
})
export class BoardListItemComponent implements OnInit {
  faGear = faGear;
  faTrashAlt = faTrashAlt;
  faPenToSquare = faPenToSquare;

  @Input() board: Board;
  @Input() index: number;
  todo: number;
  inProgress: number;
  done: number;

  isMenuOpen: boolean;
  isModalOpen: boolean;

  constructor(private store: Store<AppState>,
              private router: Router,
              private route: ActivatedRoute) {}

  ngOnInit(): void {
    this.todo = this.board.tasks.filter(task => task.status === "TODO").length;
    this.inProgress = this.board.tasks.filter(task => task.status === "IN PROGRESS").length;
    this.done = this.board.tasks.filter(task => task.status === "DONE").length;
  }

  deleteBoard() {
    this.store.dispatch(BoardActions.deleteBoard({boardId: this.board._id}));
  }

  onClick() {
    this.router.navigate([this.board._id], {relativeTo: this.route});
  }

  onSubmit(form: NgForm) {
    if (!form.valid) {
      return;
    }
    this.store.dispatch(BoardsActions.editBoard({board: {...form.value, _id: this.board._id}}));
  }
}
