import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Store} from "@ngrx/store";
import {AppState} from "../../store/app.reducer";
import {ActivatedRoute} from "@angular/router";
import {Subscription} from "rxjs";
import {Board} from "../board.model";
import {Task} from "../tasks/task.model";
import * as BoardsActions from "../store/boards.actions";
import {faBoxArchive} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-board-item',
  templateUrl: './board-item.component.html',
  styleUrls: ['./board-item.component.scss'],
})
export class BoardItemComponent implements OnInit, OnDestroy {

  constructor(private store: Store<AppState>,
              private route: ActivatedRoute) { }
  private boardSubscription: Subscription;
  private routeSubscription: Subscription;
  faBoxArchive = faBoxArchive;
  keywords: string;
  sorting: string = 'created date';
  order: 'ASC' | 'DESC' = 'ASC';
  id: string;
  board: Board | undefined;
  draggedTask: Task;
  showArchived: boolean = false;

  tasks: {
    todo: Task[],
    inProgress: Task[],
    done: Task[]
  };

  ngOnInit(): void {
    this.routeSubscription = this.route.params.subscribe(parameters => this.id = parameters['id']);
    this.boardSubscription = this.store.select('boards').subscribe(data => {
      this.board = data.boards.find(board => board._id === this.id);
      if (this.board) {
        this.tasks = {
          todo: this.board.tasks.filter(task => task.status === "TODO"),
          inProgress: this.board.tasks.filter(task => task.status === "IN PROGRESS"),
          done: this.board.tasks.filter(task => task.status === "DONE"),
        }
      }
    });
  }

  ngOnDestroy(): void {
    this.routeSubscription.unsubscribe();
    this.boardSubscription.unsubscribe();
  }

  dropTask(status: string) {
    if (this.board) {
      this.store.dispatch(BoardsActions.changeTaskStatusStart({
        boardId: this.board?._id,
        taskId: this.draggedTask._id,
        status: status
      }));
    }
  }
}
