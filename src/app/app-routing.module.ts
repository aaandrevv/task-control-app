import {NgModule} from '@angular/core';
import {PreloadAllModules, RouterModule, Routes} from '@angular/router';

const routes: Routes = [
  {
    path: '', redirectTo: 'boards', pathMatch: 'full'
  },
  {
    path: 'boards', loadChildren: () => import('../app/boards/boards.module').then(m => m.BoardsModule)
  },
  {
    path: 'boards/:id', loadChildren: () => import('../app/boards/boards.module').then(m => m.BoardsModule)
  },
  {
    path: 'authentication', loadChildren: () => import('../app/core/auth/auth.module').then(m => m.AuthModule)
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {preloadingStrategy: PreloadAllModules, onSameUrlNavigation: "reload"})],
  exports: [RouterModule]
})

export class AppRoutingModule {}
