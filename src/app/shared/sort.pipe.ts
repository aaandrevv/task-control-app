import {Pipe, PipeTransform} from '@angular/core';
import {Task} from "../boards/tasks/task.model";
import {Board} from "../boards/board.model";

@Pipe({
  name: 'sort'
})
export class SortPipe implements PipeTransform {

  transform<T extends { name: string, createdAt: Date, tasks?: Task[]}>(array: T[], sort: string, order: 'ASC' | 'DESC'): T[] {
    if (!array || !sort) {
      return array;
    }

    switch (sort) {
      case "name": {
        return [...array].sort((a, b) => order === 'ASC' ?
          a.name.localeCompare(b.name) : b.name.localeCompare(a.name));
      }
      case "created date": {
        return [...array].sort((a, b) => order === 'ASC' ?
          new Date(a.createdAt).getTime() - new Date(b.createdAt).getTime() :
          new Date(b.createdAt).getTime() - new Date(a.createdAt).getTime()
        );
      }
      case "todo": {
        return [...array].sort((a, b) => {
          if (a.tasks && b.tasks) {
            return order === 'ASC' ?
              b.tasks.filter(task => task.status === 'TODO').length - a.tasks.filter(task => task.status === 'TODO').length
              : a.tasks.filter(task => task.status === 'TODO').length - b.tasks.filter(task => task.status === 'TODO').length
              ;
          }
          return 0;
        })
      }
      case "in progress": {
        return [...array].sort((a, b) => {
          if (a.tasks && b.tasks) {
            return order === 'ASC' ?
              b.tasks.filter(task => task.status === 'IN PROGRESS').length - a.tasks.filter(task => task.status === 'IN PROGRESS').length
              : a.tasks.filter(task => task.status === 'IN PROGRESS').length - b.tasks.filter(task => task.status === 'IN PROGRESS').length
              ;
          }
          return 0;
        })
      }
      case "DONE": {
        return [...array].sort((a, b) => {
          if (a.tasks && b.tasks) {
            return order === 'ASC' ?
              b.tasks.filter(task => task.status === 'done').length - a.tasks.filter(task => task.status === 'done').length
              : a.tasks.filter(task => task.status === 'done').length - b.tasks.filter(task => task.status === 'done').length
              ;
          }
          return 0;
        })
      }
    }


    return array;
  }

}
