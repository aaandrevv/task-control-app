import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {FilterPipe} from "./filter.pipe";
import {ClickOutsideDirective} from "./click-outside.directive";
import {ModalComponent} from "./components/modal/modal.component";
import { SortPipe } from './sort.pipe';
import {FormsModule} from "@angular/forms";
import { FilterComponent } from './components/filter/filter.component';
import { SortComponent } from './components/sort/sort.component';
import {FontAwesomeModule} from "@fortawesome/angular-fontawesome";


@NgModule({
  declarations: [
    FilterPipe,
    ClickOutsideDirective,
    ModalComponent,
    SortPipe,
    FilterComponent,
    SortComponent,
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule
  ],
  exports: [
    ClickOutsideDirective,
    ModalComponent,
    FilterPipe,
    SortPipe,
    FilterComponent,
    SortComponent
  ]
})
export class SharedModule { }
