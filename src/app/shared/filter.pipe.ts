import { Pipe, PipeTransform } from '@angular/core';
import {Board} from "../boards/board.model";
import {Task} from "../boards/tasks/task.model";

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {

  transform<T extends {name: string}>(array: T[], filter: string): T[] {
    if (!filter || !array) {
      return array;
    }
    return array.filter(item => item.name.toUpperCase().includes(filter.toUpperCase()));
  }
}
