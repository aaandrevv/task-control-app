import {Component, EventEmitter, OnInit, Output} from '@angular/core';


@Component({
  selector: 'app-sort',
  templateUrl: './sort.component.html',
  styleUrls: ['./sort.component.scss']
})
export class SortComponent implements OnInit {

  sorting: string = "created date";
  order: 'ASC' | 'DESC' = 'ASC';
  @Output() sortingPicked = new EventEmitter<string>();
  @Output() orderPicked = new EventEmitter<'ASC' | 'DESC'>();

  constructor() { }

  ngOnInit(): void {
  }

}
