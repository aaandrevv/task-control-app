import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {faCircleXmark} from "@fortawesome/free-solid-svg-icons";

@Component({
  selector: 'app-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss']
})
export class ModalComponent implements OnInit {
  faCircleXMark = faCircleXmark;

  @Output() close: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor() { }

  ngOnInit(): void {
  }

  onClose() {
    this.close.emit(true);
  }
}
